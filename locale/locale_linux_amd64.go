// Code generated by 'ccgo locale/gen.c -crt-import-path "" -export-defines "" -export-enums "" -export-externs X -nostdinc -nostdlib -export-fields F -export-structs "" -export-typedefs "" -header -hide _OSSwapInt16,_OSSwapInt32,_OSSwapInt64 -ignore-unsupported-alignment -o locale/locale_linux_amd64.go -pkgname locale -Imusl-fts/ -Imusl/arch/x86_64 -Imusl/arch/generic -Imusl/obj/src/internal -Imusl/src/include -Imusl/src/internal -Imusl/obj/include -Imusl/include', DO NOT EDIT.

package locale

import (
	"math"
	"reflect"
	"sync/atomic"
	"unsafe"
)

var _ = math.Pi
var _ reflect.Kind
var _ atomic.Value
var _ unsafe.Pointer

const (
	FEATURES_H         = 0          // features.h:2:1:
	LC_ALL             = 6          // locale.h:22:1:
	LC_ALL_MASK        = 0x7fffffff // locale.h:72:1:
	LC_COLLATE         = 3          // locale.h:19:1:
	LC_COLLATE_MASK    = 8          // locale.h:69:1:
	LC_CTYPE           = 0          // locale.h:16:1:
	LC_CTYPE_MASK      = 1          // locale.h:66:1:
	LC_MESSAGES        = 5          // locale.h:21:1:
	LC_MESSAGES_MASK   = 32         // locale.h:71:1:
	LC_MONETARY        = 4          // locale.h:20:1:
	LC_MONETARY_MASK   = 16         // locale.h:70:1:
	LC_NUMERIC         = 1          // locale.h:17:1:
	LC_NUMERIC_MASK    = 2          // locale.h:67:1:
	LC_TIME            = 2          // locale.h:18:1:
	LC_TIME_MASK       = 4          // locale.h:68:1:
	X_BSD_SOURCE       = 1          // features.h:15:1:
	X_FEATURES_H       = 0          // features.h:2:1:
	X_FILE_OFFSET_BITS = 64         // <builtin>:25:1:
	X_LOCALE_H         = 0          // locale.h:2:1:
	X_LP64             = 1          // <predefined>:312:1:
	X_STDC_PREDEF_H    = 1          // <predefined>:174:1:
	X_XOPEN_SOURCE     = 700        // features.h:16:1:
	Linux              = 1          // <predefined>:255:1:
	Unix               = 1          // <predefined>:191:1:
)

type Ptrdiff_t = int64 /* <builtin>:3:26 */

type Size_t = uint64 /* <builtin>:9:23 */

type Wchar_t = int32 /* <builtin>:15:24 */

type X__int128_t = struct {
	Flo int64
	Fhi int64
} /* <builtin>:21:43 */ // must match modernc.org/mathutil.Int128
type X__uint128_t = struct {
	Flo uint64
	Fhi uint64
} /* <builtin>:22:44 */ // must match modernc.org/mathutil.Int128

type X__builtin_va_list = uintptr /* <builtin>:46:14 */
type X__float128 = float64        /* <builtin>:47:21 */

type Lconv = struct {
	Fdecimal_point      uintptr
	Fthousands_sep      uintptr
	Fgrouping           uintptr
	Fint_curr_symbol    uintptr
	Fcurrency_symbol    uintptr
	Fmon_decimal_point  uintptr
	Fmon_thousands_sep  uintptr
	Fmon_grouping       uintptr
	Fpositive_sign      uintptr
	Fnegative_sign      uintptr
	Fint_frac_digits    int8
	Ffrac_digits        int8
	Fp_cs_precedes      int8
	Fp_sep_by_space     int8
	Fn_cs_precedes      int8
	Fn_sep_by_space     int8
	Fp_sign_posn        int8
	Fn_sign_posn        int8
	Fint_p_cs_precedes  int8
	Fint_p_sep_by_space int8
	Fint_n_cs_precedes  int8
	Fint_n_sep_by_space int8
	Fint_p_sign_posn    int8
	Fint_n_sign_posn    int8
	F__ccgo_pad1        [2]byte
} /* locale.h:24:1 */

type Locale_t = uintptr /* alltypes.h:343:32 */

var _ int8 /* gen.c:2:13: */
