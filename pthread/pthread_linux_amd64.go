// Code generated by 'ccgo pthread/gen.c -crt-import-path "" -export-defines "" -export-enums "" -export-externs X -nostdinc -nostdlib -export-fields F -export-structs "" -export-typedefs "" -header -hide _OSSwapInt16,_OSSwapInt32,_OSSwapInt64 -ignore-unsupported-alignment -o pthread/pthread_linux_amd64.go -pkgname pthread -Imusl-fts/ -Imusl/arch/x86_64 -Imusl/arch/generic -Imusl/obj/src/internal -Imusl/src/include -Imusl/src/internal -Imusl/obj/include -Imusl/include', DO NOT EDIT.

package pthread

import (
	"math"
	"reflect"
	"sync/atomic"
	"unsafe"
)

var _ = math.Pi
var _ reflect.Kind
var _ atomic.Value
var _ unsafe.Pointer

const (
	CLOCKS_PER_SEC                = 1000000    // time.h:63:1:
	CLOCK_BOOTTIME                = 7          // time.h:92:1:
	CLOCK_BOOTTIME_ALARM          = 9          // time.h:94:1:
	CLOCK_MONOTONIC               = 1          // time.h:86:1:
	CLOCK_MONOTONIC_COARSE        = 6          // time.h:91:1:
	CLOCK_MONOTONIC_RAW           = 4          // time.h:89:1:
	CLOCK_PROCESS_CPUTIME_ID      = 2          // time.h:87:1:
	CLOCK_REALTIME                = 0          // time.h:85:1:
	CLOCK_REALTIME_ALARM          = 8          // time.h:93:1:
	CLOCK_REALTIME_COARSE         = 5          // time.h:90:1:
	CLOCK_SGI_CYCLE               = 10         // time.h:95:1:
	CLOCK_TAI                     = 11         // time.h:96:1:
	CLOCK_THREAD_CPUTIME_ID       = 3          // time.h:88:1:
	FEATURES_H                    = 0          // features.h:2:1:
	PTHREAD_BARRIER_SERIAL_THREAD = -1         // pthread.h:74:1:
	PTHREAD_CANCEL_ASYNCHRONOUS   = 1          // pthread.h:69:1:
	PTHREAD_CANCEL_DEFERRED       = 0          // pthread.h:68:1:
	PTHREAD_CANCEL_DISABLE        = 1          // pthread.h:65:1:
	PTHREAD_CANCEL_ENABLE         = 0          // pthread.h:64:1:
	PTHREAD_CANCEL_MASKED         = 2          // pthread.h:66:1:
	PTHREAD_CREATE_DETACHED       = 1          // pthread.h:34:1:
	PTHREAD_CREATE_JOINABLE       = 0          // pthread.h:33:1:
	PTHREAD_EXPLICIT_SCHED        = 1          // pthread.h:49:1:
	PTHREAD_H                     = 0          // pthread.h:2:1:
	PTHREAD_INHERIT_SCHED         = 0          // pthread.h:48:1:
	PTHREAD_MUTEX_DEFAULT         = 0          // pthread.h:37:1:
	PTHREAD_MUTEX_ERRORCHECK      = 2          // pthread.h:39:1:
	PTHREAD_MUTEX_NORMAL          = 0          // pthread.h:36:1:
	PTHREAD_MUTEX_RECURSIVE       = 1          // pthread.h:38:1:
	PTHREAD_MUTEX_ROBUST          = 1          // pthread.h:42:1:
	PTHREAD_MUTEX_STALLED         = 0          // pthread.h:41:1:
	PTHREAD_ONCE_INIT             = 0          // pthread.h:61:1:
	PTHREAD_PRIO_INHERIT          = 1          // pthread.h:45:1:
	PTHREAD_PRIO_NONE             = 0          // pthread.h:44:1:
	PTHREAD_PRIO_PROTECT          = 2          // pthread.h:46:1:
	PTHREAD_PROCESS_PRIVATE       = 0          // pthread.h:54:1:
	PTHREAD_PROCESS_SHARED        = 1          // pthread.h:55:1:
	PTHREAD_SCOPE_PROCESS         = 1          // pthread.h:52:1:
	PTHREAD_SCOPE_SYSTEM          = 0          // pthread.h:51:1:
	SCHED_BATCH                   = 3          // sched.h:45:1:
	SCHED_DEADLINE                = 6          // sched.h:47:1:
	SCHED_FIFO                    = 1          // sched.h:43:1:
	SCHED_IDLE                    = 5          // sched.h:46:1:
	SCHED_OTHER                   = 0          // sched.h:42:1:
	SCHED_RESET_ON_FORK           = 0x40000000 // sched.h:48:1:
	SCHED_RR                      = 2          // sched.h:44:1:
	TIMER_ABSTIME                 = 1          // time.h:98:1:
	TIME_H                        = 0          // time.h:2:1:
	TIME_UTC                      = 1          // time.h:65:1:
	X_BSD_SOURCE                  = 1          // features.h:15:1:
	X_FEATURES_H                  = 0          // features.h:2:1:
	X_FILE_OFFSET_BITS            = 64         // <builtin>:25:1:
	X_LP64                        = 1          // <predefined>:312:1:
	X_PTHREAD_H                   = 0          // pthread.h:2:1:
	X_SCHED_H                     = 0          // sched.h:2:1:
	X_STDC_PREDEF_H               = 1          // <predefined>:174:1:
	X_TIME_H                      = 0          // time.h:2:1:
	X_XOPEN_SOURCE                = 700        // features.h:16:1:
	Linux                         = 1          // <predefined>:255:1:
	Unix                          = 1          // <predefined>:191:1:
)

type Ptrdiff_t = int64 /* <builtin>:3:26 */

type Size_t = uint64 /* <builtin>:9:23 */

type Wchar_t = int32 /* <builtin>:15:24 */

type X__int128_t = struct {
	Flo int64
	Fhi int64
} /* <builtin>:21:43 */ // must match modernc.org/mathutil.Int128
type X__uint128_t = struct {
	Flo uint64
	Fhi uint64
} /* <builtin>:22:44 */ // must match modernc.org/mathutil.Int128

type X__builtin_va_list = uintptr /* <builtin>:46:14 */
type X__float128 = float64        /* <builtin>:47:21 */

type Time_t = int64 /* alltypes.h:85:16 */

type Clockid_t = int32 /* alltypes.h:214:13 */

type Timespec = struct {
	Ftv_sec  Time_t
	Ftv_nsec int64
} /* alltypes.h:229:1 */

type Pthread_t = uintptr /* alltypes.h:273:26 */

type Pthread_once_t = int32 /* alltypes.h:279:13 */

type Pthread_key_t = uint32 /* alltypes.h:284:18 */

type Pthread_spinlock_t = int32 /* alltypes.h:289:13 */

type Pthread_mutexattr_t = struct{ F__attr uint32 } /* alltypes.h:294:37 */

type Pthread_condattr_t = struct{ F__attr uint32 } /* alltypes.h:299:37 */

type Pthread_barrierattr_t = struct{ F__attr uint32 } /* alltypes.h:304:37 */

type Pthread_rwlockattr_t = struct{ F__attr [2]uint32 } /* alltypes.h:309:40 */

type X__sigset_t = struct{ F__bits [16]uint64 } /* alltypes.h:349:9 */

type Sigset_t = X__sigset_t /* alltypes.h:349:71 */

type Pthread_attr_t = struct {
	F__u struct {
		F__ccgo_pad1 [0]uint64
		F__i         [14]int32
	}
} /* alltypes.h:372:147 */

type Pthread_mutex_t = struct {
	F__u struct {
		F__ccgo_pad1 [0]uint64
		F__i         [10]int32
	}
} /* alltypes.h:377:157 */

type Pthread_cond_t = struct {
	F__u struct {
		F__ccgo_pad1 [0]uint64
		F__i         [12]int32
	}
} /* alltypes.h:387:112 */

type Pthread_rwlock_t = struct {
	F__u struct {
		F__ccgo_pad1 [0]uint64
		F__i         [14]int32
	}
} /* alltypes.h:397:139 */

type Pthread_barrier_t = struct {
	F__u struct {
		F__ccgo_pad1 [0]uint64
		F__i         [8]int32
	}
} /* alltypes.h:402:137 */

type Pid_t = int32 /* alltypes.h:235:13 */

type Sched_param = struct {
	Fsched_priority int32
	F__reserved1    int32
	F__reserved2    [2]struct {
		F__reserved1 Time_t
		F__reserved2 int64
	}
	F__reserved3 int32
	F__ccgo_pad1 [4]byte
} /* sched.h:19:1 */

type Timer_t = uintptr /* alltypes.h:209:14 */

type Clock_t = int64 /* alltypes.h:219:14 */

type Locale_t = uintptr /* alltypes.h:343:32 */

type Tm = struct {
	Ftm_sec      int32
	Ftm_min      int32
	Ftm_hour     int32
	Ftm_mday     int32
	Ftm_mon      int32
	Ftm_year     int32
	Ftm_wday     int32
	Ftm_yday     int32
	Ftm_isdst    int32
	F__ccgo_pad1 [4]byte
	Ftm_gmtoff   int64
	Ftm_zone     uintptr
} /* time.h:38:1 */

type Itimerspec = struct {
	Fit_interval struct {
		Ftv_sec  Time_t
		Ftv_nsec int64
	}
	Fit_value struct {
		Ftv_sec  Time_t
		Ftv_nsec int64
	}
} /* time.h:80:1 */

type X__ptcb = struct {
	F__f    uintptr
	F__x    uintptr
	F__next uintptr
} /* pthread.h:206:1 */

var _ int8 /* gen.c:2:13: */
